<!DOCTYPE html>
<html>
<head>
	<title>Services</title>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-167530546-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-167530546-1');
	</script>

	<!-- Keywords -->
	<meta name="keywords" content="decorators, interior, exterior, builders, construction, va, VA, Virginia, 
		Northern Virginia, NoVA, NOVA, rennovation, addition, bathroom, bedroom, roof, backyard, front yard, 
		sheds, install, open space, space, garden, roofing, floor, flooring, tiles, tiling, paint, painting, 
		spring, special, custom, ideal, modern, traditional, style, stylish, update, decor, colonial, shingles, 
		baseboards, deck, siding, downsprout, storm drain, kbdecorators, knickers, knicker, knickerbocker, 
		knickerbocker decorators, knicker bocker, zero, 1, 2, 3, 4, beds, baths, rooms, ceiling, townhouse, house, 
		condo, apartment, fixture, repair, replace, replacement, fixing, budget friendly, budget, affordable, competitive, 
		customer, satisfaction, customer satisfaction, licensed, bonded, insured, return customers, family owned.">

	<!-- 	viewport -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- 	Font awesome and SuperSlides -->
	<script src="https://kit.fontawesome.com/e294a45d38.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="css/all.min.css">
	<link rel="stylesheet" type="text/css" href="css/superslides.css">
	<!-- Bootstrap CDN -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<!-- Jquery CDN -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
	
</head>
<body>

	<div class="loader">
		<div class="inner">

		</div>
	</div>

	<?php
		require "customer-nav.php";
	?>

	<div id ="services">
		<div class="container">
			<h1>Request Services</h1>
			<form method="post" action="received.php">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="First Name">First Name</label>
						<input type="text" class="form-control" name="fname" id="inputFName" placeholder="First Name">
					</div>
					<div class="form-group col-md-6">
						<label for="Last Name">Last Name</label>
						<input type="text" class="form-control" name="lname" id="inputLName" placeholder="Last Name">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="Phone Number">Phone Number</label>
						<input type="tel" class="form-control" name="phone" id="inputPhone" placeholder="Phone Number" pattern="[0-9]{3}[0-9]{3}[0-9]{4}|[0-9]{3}-[0-9]{3}-[0-9]{4}">
					</div>
					<div class="form-group col-md-6">
						<label for="Email">Email</label>
						<input type="email" class="form-control" name="email" id="inputEmail" placeholder="Email"
							pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="Street">Street</label>
						<input type="text" class="form-control" name="street" id="inputStreet" placeholder="Street">
					</div>
					<div class="form-group col-md-6">
						<label for="City">City</label>
						<input type="text" class="form-control" name="city" id="inputCity" placeholder="City">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="Zip Code">Zip Code</label>
						<input type="text" class="form-control" name="zip" id="inputZIP" placeholder="Zip Code">
					</div>
					<div class="form-group col-md-6">
						<label for="inputType">Type of Work</label>
						<select name="type" id="inputState" class="form-control" placeholder="Choose">
							<option selected>Choose...</option>
							<option>Kitchen Renovation</option>
							<option>Bathroom Renovation</option>
							<option>Crown Molding</option>
							<option>Tiling</option>
							<option>Window/Door Installation</option>
							<option>Painting</otion>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="exampleFormControlTextarea1">Details</label>
					<textarea class="form-control" name="details" id="exampleFormControlTextarea1" rows="5"></textarea>
				</div>
				<br>
				<input type="hidden" name="hidden" value="TRUE" />
				<button type="submit" name="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>

	<script src="js/typed.min.js"></script>
	<script src="js/jquery.superslides.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<script type="text/javascript" src="https://unpkg.com/isotope-layout@3.0.6/dist/isotope.pkgd.min.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
	<script src="js/script.js"></script>

</body>

<footer>
	<div class = "copyright">
		<div class = "col-md-12 text-center">
			<p>&copy; Copyright 2020. Zero Index Solutions. All Rights Reserved.</p>
		</div>
	</div>
</footer>

</html>