<?php session_start();
 if (!isset($_SESSION['empID']))
 {
 	header("Location: emp.php");
 	die();
 }
 
 // Deleting action for inv items
 if(isset($_POST['delete-item'])){
	require 'database/config.php';
	if(!empty($_POST['checkbox'])) {
		foreach($_POST['checkbox'] as $check) {
			
			$sql = "DELETE FROM item WHERE itemCode = '$check';";

			if (mysqli_query($conn, $sql)) {
				
			} else {
			}
		}
		mysqli_close($conn);
		header("Location: inv.php");
		exit(); 
	}
	else {
		echo "<div id='snackbar'>No Item Selected</div>";
	}
 }
 
 // Deleting action for inv storage
 if(isset($_POST['delete-store'])){
	require 'database/config.php';
	if(!empty($_POST['checked-store'])) {
		foreach($_POST['checked-store'] as $selected) {			
			$sql = "DELETE FROM storage WHERE storeID = '$selected';";
			//The following updates all items that are stored in the deleted storage into unassigned
			$sql2 = "UPDATE item SET storeID = 1 WHERE storeID= '$selected'";
			if (mysqli_query($conn, $sql2)) {
				
			} 
			else {
			}
			
			if (mysqli_query($conn, $sql)) {
				
			} 
			else {
				echo "Error altering table: " . mysqli_error($conn);
			}
		}	
		mysqli_close($conn);
		header("Location: inv.php");
		exit(); 
	}
	else {
		echo "<div id='snackbar'>No Storage Selected</div>";
	}
 }
 
//Update action for inv items
//Consider finding a more efficient method, currently copies every record data and reposts both the changed data and unchanged
if(isset($_POST['update-item'])) {
	require 'database/config.php';
	$itemCodes = isset($_POST['itemCodes']) ? $_POST['itemCodes'] : '';
	$quantities = isset($_POST['quantities']) ? $_POST['quantities'] : '';
	$storeUnits = isset($_POST['storeUnits']) ? $_POST['storeUnits'] : '';
	$suppliers = isset($_POST['suppliers']) ? $_POST['suppliers'] : '';
	$index = 0;
	if(!empty($itemCodes)) {
		foreach($itemCodes as $code) {
			$sql = "UPDATE item SET quantity = $quantities[$index], storeID = $storeUnits[$index], supplier = '$suppliers[$index]' WHERE itemCode = $code";
			$index++;
			
			if (mysqli_query($conn, $sql)) {
				
			} else {
				echo "Error updating record: " . mysqli_error($conn);
			}
		}
		mysqli_close($conn);
		header("Location: inv.php");
		exit(); 
	}
	else {
		echo "<div id='snackbar'>ERROR: Something went wrong with update</div>";
	}
}

//Update action for inv storage
//Consider finding a more efficient method, currently copies every record data and reposts both the changed data and unchanged
if(isset($_POST['update-store'])) {
	require 'database/config.php';
	$storeIDs = isset($_POST['storeIDs']) ? $_POST['storeIDs'] : '';
	$storageUnits = isset($_POST['storageUnits']) ? $_POST['storageUnits'] : '';
	$addresses = isset($_POST['addresses']) ? $_POST['addresses'] : '';
	$index = 0;
	if(!empty($storeIDs)) {
		foreach($storeIDs as $SID) {
			$sql = "UPDATE storage SET storeUnit = '$storageUnits[$index]', address = '$addresses[$index]' WHERE storeID = $SID";
			$index++;
			
			if (mysqli_query($conn, $sql)) {
				echo "Record(s) updated successfully";
				
			} else {
				echo "Error updating record: " . mysqli_error($conn);
			}
		}
		mysqli_close($conn);
		header("Location: inv.php");
		exit(); 
	}
	else {
		echo "<div id='snackbar'>ERROR: Something went wrong with update</div>";
	}
}
 ?>
 <html>

 <head>
 	<title>Emp-KbDecorators</title>

 	<!-- Global site tag (gtag.js) - Google Analytics -->
 	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-167530546-1"></script>
 	<script>
 		window.dataLayer = window.dataLayer || [];
 		function gtag(){dataLayer.push(arguments);}
 		gtag('js', new Date());

 		gtag('config', 'UA-167530546-1');
 	</script>

 	<!--Prevents submit data from forms being submitted again when page is refreshed-->
 	<script>
 		if(window.history.replaceState) {
 			window.history.replaceState(null, null, window.location.href);
 		}
 	</script>

 	<!-- 	viewport -->
 	<meta charset="utf-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1">

 	<!-- 	Font awesome and SuperSlides -->
 	<script src="https://kit.fontawesome.com/e294a45d38.js" crossorigin="anonymous"></script>
 	<link rel="stylesheet" href="css/all.min.css">
 	<!-- Bootstrap CDN -->
 	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
 	<!-- Jquery CDN -->
 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />	
 </head>

 <body>
 	<div class="loader">
 		<div class="inner">
 		</div>
 	</div>

 	<?php
		require "emp-nav.php";
	?>

 	<?php 	
 	if (isset($_SESSION['empID'])){	

		include("database/config.php");
		
		//Runs if the item modal form was submitted successfully
 		if(isset($_POST['submit-item'])) {
 			$quantity = isset($_POST['quantity']) ? $_POST['quantity'] : '';
 			$itemName = isset($_POST['itemName']) ? $_POST['itemName'] : '';
 			$consumable = isset($_POST['consumable']) ? $_POST['consumable'] : '';
 			$itemDesc = isset($_POST['itemDesc']) ? $_POST['itemDesc'] : '';
 			$category = isset($_POST['category']) ? $_POST['category'] : '';
 			$storage = isset($_POST['storage']) ? $_POST['storage'] : '';
 			$supplier = isset($_POST['supplier']) ? $_POST['supplier'] : '';

			//Creates a new item record in the item table based on the submitted form, fields in order of item table
 			$sql = "INSERT INTO item (storeID, consumable, name, description, category, quantity, supplier) VALUES ($storage, $consumable, '$itemName', '$itemDesc', '$category', $quantity, '$supplier')";
 			if(!mysqli_query($conn, $sql)) {
 				echo "Error: " . $sql . "<br>" . mysqli_error($conn);
 			}
 		}

		//Runs if the storage form was submitted successfully
 		if(isset($_POST['submit-store'])) {
 			$unit = isset($_POST['unit']) ? $_POST['unit'] : '';
 			$address = isset($_POST['address']) ? $_POST['address'] : '';

			//Creates a new storage record in the storage table based on the submitted form
 			$sql = "INSERT INTO storage (storeUnit, address) VALUES ('$unit', '$address')";
 			if(!mysqli_query($conn, $sql)) {
 				echo "Error: " . $sql . "<br>" . mysqli_error($conn);
 			}
 		}

 		?>

		<!--start item modal-->
 		<div class="modal fade" id="itemModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
 			<div class="modal-dialog modal-dialog-centered" role="document">
 				<div class="modal-content">
 					<div class="modal-header">
 						<h5 class="modal-title" id="exampleModalLongTitle">Add New Item</h5>
 						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 							<span aria-hidden="true">&times;</span>
 						</button>
 					</div>
 					<div class="modal-body">

 						<!--Start add item modal contents-->
 						<form class="item-form" action="" method="post">
							<div class="form-group">
								<label for="quantity">Quantity: <b class="required-asterisk">*</b></label>
								<input class="quantity-form form-control" type="number" name="quantity" min="1" value="1" required>
							</div>
							
							<div class="form-group">
								<label for="itemName">Item name: <b class="required-asterisk">*</b></label>
								<input type="text" class="form-control" id="itemName" name="itemName" maxlength="30" required>
							</div>
														
							<div class="form-group">
								<label for="consumable">Consumable: <b class="required-asterisk">*</b></label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" name="consumable" type="radio" id="consumable-yes" value=true required>
									<label class="form-check-label" for="consumable-yes">yes</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" name="consumable" type="radio" id="consumable-no" value=false required>
									<label class="form-check-label" for="consumable-no">no</label>
								</div>
							</div>
							
							<div class="form-group">
								<label for="description">Item Description: </label>
								<textarea class="form-control" id="description" type="text" rows="3" cols="40" name="itemDesc" maxlength="50"></textarea><br>
							</div>
							
							<div class="form-group">
 							<label for="category">Category: <b class="required-asterisk">*</b></label>
								<select name="category" class="form-control" required>
									<option></option>
									<option value="tool">Tool</option>
									<option value="fastener">Fastener</option>
									<option value="wood">Wood/ply</option>
									<option value="stone">Stone/slab</option>
									<option value="other">Other</option>
								</select>
							</div>

							<div class="form-group">
								<label for="supplier">Supplier: </label>
								<input class="form-control" type="text" name="supplier" maxlength="30"></textarea>
							</div>
							
							<div class="form-group">
								<label for="storage">Storage Unit: </label>
								<select class="form-control" type="number" name="storage">
									<option></option>
									<!--The following PHP creates the drop down options for types of storage and posts the storage id based on the storage type selected-->
									<?php
									$sql = "SELECT storeID, storeUnit FROM storage";
									$result = mysqli_query($conn, $sql) or die;

									while($row = mysqli_fetch_array($result)) {
										echo "<option value=\"" . $row['storeID'] . "\">" . $row['storeUnit'] . "</option>";
									}
									?>
								</select>
								<?php
								mysqli_free_result($result);
								?>
							</div>
 							<input type="reset" value="Clear Form" />
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<input type="submit" name="submit-item" value="submit" class="btn btn-primary"/>
							</div>
 						</form>
 						<!--end add item modal contents-->

 					</div>	
 				</div>
 			</div>
 		</div>
 		<!--End item Modal-->

 		<!--Start storage Modal-->
 		<div class="modal fade" id="storageModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
 			<div class="modal-dialog modal-dialog-centered" role="document">
 				<div class="modal-content">
 					<div class="modal-header">
 						<h5 class="modal-title" id="exampleModalLongTitle">Modify Storage</h5>
 						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 							<span aria-hidden="true">&times;</span>
 						</button>
 					</div>
 					<div class="modal-body">

 						<!--Start modify storage modal contents-->
 						<form class="store-form" action="" method="post">
							<div class="form-group">
								<label for="unit">Storage Unit: <b class="required-asterisk">*</b></label>
								<input type="text" class="form-control" id="unit" name="unit" maxlength="15" required>
							</div>
							
							<div class="form-group">
								<label for="address">Address: </label>
								<input type="text" class="form-control" id="address" name="address" maxlength="30">
							</div>
							<div class="table-clear-submit">
								<input type="reset" value="Clear Form" />
								<input type="submit" value="submit" name="submit-store" class="btn btn-primary"/>
							</div>
							
							<!--update/delete buttons-->
							<div class="table-buttons">
								<div class="row">
									<div class="text-left col-3"> 
										<form>
											<button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
										</form>
									</div>
									<div class="text-right col-9"> 
										<form action="inv.php" method="POST">
											<button onClick="return confirm('Are you sure you want to update? Some items may be deleted!')" type="submit" form="mod-store" value="Submit" class="btn btn-warning" name="update-store">Update</button>
											<button onClick="return confirm('Are you sure you want to delete?')" type="submit" form="mod-store" value="Submit" class="btn btn-danger" name="delete-invent">Delete</button>
										</form>
									</div>
								</div>
							</div>
							<!--end update-delete buttons-->
							
 						</form>

 						<!--Start storage table-->
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover table-sm">
								<thead class="thead-dark">
									<tr>
										<th>Store ID</th>
										<th>Store Unit</th>
										<th>Address</th>
										<th>Select</th>
									</tr>
								</thead>
								<tbody>
									<form method="POST" action="inv.php" id="mod-store">
										<?php
										$sql = "SELECT*FROM storage";
										$result = mysqli_query($conn, $sql) or die;

										while($row = mysqli_fetch_assoc($result)) {
											?>
											<tr>										
											<?php
											//Prevents the first storage element from being deleted as it is set for items that have an unassigned location
											if($row['storeID'] == 1) {
												?>
												<td><?php echo $row['storeID']; ?></td>
												<td><?php echo $row['storeUnit']; ?></td>
												<td></td>
												<td></td>
												<?php
											}
											else {
												?>
												<td><input type="hidden" value="<?php echo $row['storeID']; ?>" name="storeIDs[]"><?php echo $row['storeID']; ?></td>
												<td><input class="table-form-text" type="text" value="<?php echo $row['storeUnit']; ?>" name="storageUnits[]"></td>
												<td><input class="table-form-text" type="text" value="<?php echo $row['address']; ?>" name="addresses[]"></td>
												<td><input type='checkbox' value="<?php echo $row['storeID']; ?>" name="checked-store[]"></td>
											<?php
											}
											?>
											</tr>
										<?php
										}
										mysqli_free_result($result);
										?> 
									</form>
								</tbody>
							</table>
						</div>
 						<!--end storage table-->
						
 					</div>
 				</div>
 			</div>
 		</div>
		<!--end storage modal-->
		
		<!--begin new database record snackbar display-->
		<?php
		//displays the new item that was added
		if(isset($_POST['submit-item'])) {
			?>
			<div class="text-center" id='snackbar'>
				<h3 style="text-align:center;">The following item was added</h3>
				<div id="inv">		
					<div class="container">
						<div class="table-responsive">
							<table class="table table-dark table-hover table-sm">
								<thead>
									<tr>
										<th scope="col">Name</th>
										<th scope="col">Quantity</th>
										<th scope="col">Store ID</th>
									</tr>
								</thead>
								<tbody>
									<?php
									echo "<tr>";
									echo "<td>$itemName</td>";
									echo "<td>$quantity</td>";
									echo "<td>$storage</td>";
									echo "</tr>";
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
		//displays the new storage that was added-->
		if(isset($_POST['submit-store'])) {
			?>
			<div class="text-center" id='snackbar'>
				<h3 style="text-align:center;">The following storage unit was added</h3>
				<div id="inv">		
					<div class="container">
						<div class="table-responsive">
							<table class="table table-dark table-hover table-sm">
								<thead>
									<tr>
										<th scope="col">Storage Type</th>
										<th scope="col">Address</th>
									</tr>
								</thead>
								<tbody>
									<?php
									echo "<tr>";
									echo "<td>$unit</td>";
									echo "<td>$address</td>";
									echo "</tr>";
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div
			<?php
		}
		?>
		<!--end new database record snackbar display-->

 		<h1 class="text-center">Inventory Management</h1>

		<!--buttons for modals-->
		<div class="modal-buttons">
			<div class="text-center"> 
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#itemModalCenter">Add New Item</button>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#storageModalCenter">Modify Storage</button>
			</div>
		</div>
		
 		<!--Start inventory items table and search components-->	
		<div class="container-fluid">
			<!--Search component-->
			<form class="search-invent" action="" method="post" id="inv-search-form">
				<h5 class="text-center">Search Items</h5>
				<div class="search-bar">
					<input class="form-control" type="text" name="name-search" placeholder="Search" aria-label="Search">
				</div>
				<div class="filter-content">
					<div class="container-fluid">	
						<div class="row">
							<div class="text-center col-sm">
							<label for="category-search">Category:</label>
								<select name="category-search">
									<option></option>
									<option value="tool">Tool</option>
									<option value="fastener">Fastener</option>
									<option value="wood">Wood/ply</option>
									<option value="stone">Stone/slab</option>
									<option value="other">Other</option>
								</select>
							</div>
							
							<div class="text-center col-sm">
								<label for="consumable-search">Consumable:</label>
								<input type="radio" name="consumable-search" value=true> yes
								<input type="radio" name="consumable-search" value=false> no
							</div>
							
							<div class="text-center col-md">
								<label for="storage">Storage:</label>
								<select type="number" name="storage-search">
									<option></option>
									<!--The following PHP creates the drop down options for types of storage and posts the storage id based on the storage type selected-->
									<?php
									$sql = "SELECT storeID, storeUnit FROM storage";
									$result = mysqli_query($conn, $sql) or die;

									while($row = mysqli_fetch_array($result)) {
										echo "<option value=\"" . $row['storeID'] . "\">" . $row['storeUnit'] . "</option>";
									}
									?>
								</select>	
							</div>
						</div>
					</div>
				</div>
				
				<div class="text-center">
					<input type="reset" value="Clear Form" />
					<button class="btn btn-success" name="submit-search" type="submit" form="inv-search-form">Search</button>
				</div>
			</form>
			<!--End search component-->
			
			<!--update/delete/reset buttons-->
			<div class="table-buttons">
				<div class="row">
					<div class="text-left col-3"> 
						<form>
							<button id="reset-inv-btn" class="btn btn-dark" action="" type="submit">Reset</button>
						</form>
					</div>
					<div class="text-right col-9"> 
						<form action="inv.php" method="POST">
							<button onClick="return confirm('Are you sure you want to update? Some items may be deleted!')" type="submit" form="mod-inv" value="Submit" class="btn btn-warning" name="update-item">Update</button>
							<button onClick="return confirm('Are you sure you want to delete?')" type="submit" form="mod-inv" value="Submit" class="btn btn-danger" name="delete-item">Delete</button>
						</form>
					</div>
				</div>
			</div>
			<!--end update/delete/reset buttons-->
		
			<!--start item inventory table-->
			<div class="table-responsive">
				<table class="table table-striped table-hover table-sm table-bordered">
					<thead class="thead-dark">
						<tr>
							<th scope="col">Item Code</th>
							<th scope="col">Name</th>
							<th scope="col">Quantity</th>
							<th scope="col">Storage Unit</th>
							<th scope="col">Supplier</th>
							<th scope="col">Select</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$sql="SELECT itemCode, name, quantity, consumable, storeUnit, address, description, item.storeID, supplier 
							  FROM item LEFT JOIN storage ON item.storeID = storage.storeID";
						
						if(isset($_POST['submit-search'])) {
							$nameSearch = isset($_POST['name-search']) ? $_POST['name-search'] : '';
							$consumeSearch = isset($_POST['consumable-search']) ? $_POST['consumable-search'] : '';
							$catSearch = isset($_POST['category-search']) ? $_POST['category-search'] : '';
							$storeSearch = isset($_POST['storage-search']) ? $_POST['storage-search'] : '';
							//variable if true determines the use of AND for sql query
							$useAND = false;
							
							//Adds to the query to filter what is displayed based on user search input
							if($nameSearch != null) {
								$sql .= " WHERE UPPER(name) LIKE UPPER('%{$nameSearch}%')";
								$useAND = true;
							}
							if($consumeSearch != null) {
								if($useAND) {
									$sql .= " AND consumable IS $consumeSearch";
								}
								else {
									$sql .= " WHERE consumable IS $consumeSearch";
									$useAND = true;
								}
							}
							if($catSearch != null) {
								if($useAND) {
									$sql .= " AND UPPER(category) = UPPER('{$catSearch}')";
								}
								else {
									$sql .= " WHERE UPPER(category) = UPPER('{$catSearch}')";
									$useAND = true;
								}
							}
							if($storeSearch != null) {
								if($useAND) {
									$sql .= " AND item.storeID = $storeSearch";
								}
								else {
									$sql .= " WHERE item.storeID = $storeSearch";
									$useAND = true;
								}
							}
						}
						//last line of query which orders items by item code
						$sql .= " ORDER BY itemCode";

						$result=mysqli_query($conn, $sql);
						
						//Creates the item inventory table
						if(mysqli_num_rows($result) > 0) {
							echo "<form action='inv.php' method='POST' id ='mod-inv'>";
							while ($row = mysqli_fetch_assoc($result)) {
								?>
								<tr>
									<th scope='row'><input type="hidden" value="<?php echo $row['itemCode']; ?>" name="itemCodes[]"><?php echo $row['itemCode']; ?></th>
									<?php
									//sets yes/no output for consumable
									if ($row['consumable'] == 0) {
										$consumeOutput = "No";
									}
									else {
										$consumeOutput = "Yes";
									}
									?>
									<td title="<?php echo "Consumable: " . $consumeOutput . "\n" . $row['description']; ?>"><?php echo $row['name']; ?></td>
									
									<td><input class="quantity-form" type="number" min="0" value="<?php echo $row['quantity']; ?>" name="quantities[]"></td>
									
									<!--Displays the storage unit for the item and has a drop down with other storage units which can be updated-->
									<td title="<?php echo $row['address']; ?>"><select type="number" name="storeUnits[]">
										<option value="<?php echo $row['storeID']; ?>"><?php echo $row['storeUnit']; ?></option>
										<!--The following PHP creates the drop down options for types of storage and posts the storage id based on the storage type selected-->
										<?php
										$storeQuery = "SELECT storeID, storeUnit FROM storage";
										$storeResult = mysqli_query($conn, $storeQuery) or die;

										while($row2 = mysqli_fetch_array($storeResult)) {
											echo "<option value=\"" . $row2['storeID'] . "\">" . $row2['storeUnit'] . "</option>";
										}
										?>
										</select><br>
										<?php
										mysqli_free_result($storeResult);
										?>
									</td>
									
									<td><input class="table-form-text" type ="text" value="<?php echo $row['supplier']; ?>" name="suppliers[]"></td>
									
									<td><input type="checkbox" class="form" value="<?php echo $row['itemCode']; ?>" name ="checkbox[]"></td>
								</tr>
							<?php
							}
							?>
							</form>
						<?php
						}
						else {
							echo "<h5>No data was found</h5>";
						}
						
						mysqli_free_result($result);
						?>
				</tbody>
			</table>
		</div>
		<!--end item inventory table-->
		
	</div>
	<!--end item inventory table and search components-->
	
	<?php
	mysqli_close($conn);
}
else {
	echo '<h1 style="color:blue;text-align:center;margin-top:100px;">You are not signed in</h1>';
}
?>

<div class = "copyright">
	<div class = "col-md-12 text-center">
		<p>&copy; Copyright 2020. Zero Index Solutions. All Rights Reserved.</p>
	</div>
</div>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<script src="js/script.js"></script>

</body>
</html>