<!DOCTYPE html>
<html>
<head>
	<title>Services</title>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-167530546-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-167530546-1');
	</script>

	<!-- 	viewport -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- 	Font awesome and SuperSlides -->
	<script src="https://kit.fontawesome.com/e294a45d38.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="css/all.min.css">
	<link rel="stylesheet" type="text/css" href="css/superslides.css">
	<!-- Bootstrap CDN -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<!-- Jquery CDN -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

	<script>
		if ( window.history.replaceState ) {
			window.history.replaceState( null, null, window.location.href );
		}
	</script>
	
</head>
<body>

	<div class="loader">
		<div class="inner">

		</div>
	</div>

	<?php
		require "customer-nav.php";
	?>

	<div id ="services">
		<div class="container">
			<?php
			include("database/config.php");

                //checks to see that each input form was filled out
			if (empty($_POST['fname']) || empty($_POST['lname']) || empty($_POST['phone']) || empty($_POST['email']) || empty($_POST['street']) || empty($_POST['city']) || empty($_POST['zip']) || $_POST['type'] == 'Choose...' || empty($_POST['details'])) {
				echo "<h1>Unfortunately your request could not be submitted</h1>";
				echo "<h3><a href=\"https://kbdecorators.com/services.php\">Click Here To Try Again</a></h3>";
			}
			else { 
                    //creates variables with entered data
				$fname=$_POST['fname'];
				$lname=$_POST['lname'];
				$phone=$_POST['phone'];
				$email=$_POST['email'];
				$street=$_POST['street'];
				$city=$_POST['city'];
				$zip=$_POST['zip'];
				$type=$_POST['type'];
				$details=$_POST['details'];
                    //creates insert statement for the customers table
				$customersInsertQuery = "INSERT INTO customers (lname, fname, email, phoneNum) VALUES ('$lname', '$fname', '$email', '$phone');";
                    //executes the insert statement for the customer table
				if (mysqli_query($conn, $customersInsertQuery)) {
                        //creates a variable for the cusID created from statement above
					$cusID=mysqli_insert_id($conn);
                        //creates insert statement for the jobs table
					$jobsInsertQuery = "INSERT INTO jobs (type, description, street, city, zip, requestdate, cusID) VALUES ('$type', '$details', '$street', '$city', '$zip', CURRENT_DATE(), '$cusID');";
                        //executes the insert statement for the jobs table
					if (mysqli_query($conn, $jobsInsertQuery)) {
						echo "<h1>Thank you<br>Your submission is currently under review</h1>";
						echo "<h3><a href=\"https://kbdecorators.com\">Click Here To Go Home</a></h3>";
					}
					else {
						echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                            //creates delete statement to remove customer record added above
						$customerDeleteQuery="DELETE FROM customers WHERE cusID = '$cusID';";
                            //executes delete statement for the customers table
						if (mysqli_query($conn, $customerDeleteQuery)) {
							echo "<p>Customer removed from db</p>";
						}
						else {
							echo "Error: " .$sql . "<br>" . mysqli_error($conn);
						}
					}
				}
                    //executes if the customer was not added to the db
				else {
					echo "Error: " . $sql . "<br>" . mysqli_error($conn);
				}
			}
			mysqli_close($conn);
			?>
		</div>
	</div>
	
	<script src="js/typed.min.js"></script>
	<script src="js/jquery.superslides.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<script type="text/javascript" src="https://unpkg.com/isotope-layout@3.0.6/dist/isotope.pkgd.min.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
	<script src="js/script.js"></script>

</body>

<footer>
	<div class = "copyright">
		<div class = "col-md-12 text-center">
			<p>&copy; Copyright 2020. Zero Index Solutions. All Rights Reserved.</p>
		</div>
	</div>
</footer>

</html>