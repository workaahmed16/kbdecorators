<?php 

//Checking that the login button was pressed
if (isset($_POST['login_submit'])) {

	require 'database/config.php';

	$email = $_POST['email'];
	$pass = $_POST['password'];

	//Checking for empty fields
	if (empty($email) || empty($pass)) {
		header("Location: emp.php?error=emptyfields");
		exit();
	}else{

		//SQL statement checking if the email is already in the database. 
		$sql = "SELECT * FROM employees WHERE email=?;";

		//Connecting to MySql database
		$statement = mysqli_stmt_init($conn);
		

		//Error checking if statement to determine whether connection to db was succcessful.
		if (!mysqli_stmt_prepare($statement, $sql)) {
			header("Location: emp.php?error=sqlerror");
			exit();
		}else{
			mysqli_stmt_bind_param($statement, "s", $email);
			mysqli_stmt_execute($statement);
			$result = mysqli_stmt_get_result($statement);
			
			//Checking whether we have that email in the database
			if ($row = mysqli_fetch_assoc($result)) {

				//Fetching password from db and decrypting it in order to do a string comparison.
				$currentEmail = $row['email'];
				$sqlTwo = "SELECT email, aes_decrypt(password, 'H+MbQeThWmZq3t6w') from employees where email = '$currentEmail';";
				$dbPass = mysqli_query($conn, $sqlTwo);
				$currentPass = $dbPass->fetch_array()[1] ?? '';

				$pwdCheck = strcmp($pass, $currentPass); 

				if ($pwdCheck == 0) {
					session_start();
					$_SESSION['empID'] = $row['empID'];
					header("Location: home.php?login=successful");
					exit();
				}else{
					header("Location: emp.php?error=wrongpassword");
					exit();
				}
			}else{
				header("Location: emp.php?error=nouser");
				exit(); 
			}
		}
	}

}else{
	header("Location: emp.php");
	exit();
}

?>