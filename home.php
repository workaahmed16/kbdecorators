<?php 
session_start();
if (!isset($_SESSION['empID']))
{
	header("Location: emp.php");
	die();
}

	// Snackbar error messages for when password updating fails
if (isset($_GET['error'])) {
	if ($_GET['error'] == "passwords_dont_match"){
		echo "<div id='snackbar'>ERROR: Passwords do not match</div>";
	}else if ($_GET['error'] == "current_pass_wrong"){
		echo "<div id='snackbar'>ERROR: Wrong current password</div>";
	}
}

if (isset($_GET['success'])) {
	if ($_GET['success'] == "pwUpdated"){
		echo "<div id='snackbar'>SUCCESS: Password Updated Successfully</div>";
	}else if ($_GET['error'] == "current_pass_wrong"){
		echo "<div id='snackbar'>ERROR: Password not updated successfully, please try again</div>";
	}
}
?>

<html>
<head>
	<title>Emp-KbDecorators</title>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-167530546-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-167530546-1');
	</script>

	<!-- 	viewport -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- 	Font awesome and SuperSlides -->
	<script src="https://kit.fontawesome.com/e294a45d38.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="css/all.min.css">
	<!-- Bootstrap CDN -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<!-- Jquery CDN -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
	
</head>
<body>

	<div class="loader">
		<div class="inner">

		</div>
	</div>

	<?php
		require "emp-nav.php";
	?>

	<?php 

	if (isset($_SESSION['empID'])){
		include("database/config.php");
		$empID = $_SESSION['empID'];
		$query="select * from employees where empID='$empID'";
		$sql=mysqli_query($conn, $query);

		$fname = $lname = $empID = $email = $phoneNum = $password = $empSince = '';
		//Grabbing the results from the mySQL query. 
		while ($row = mysqli_fetch_assoc($sql)) {
			$fname = $row['fname'];
			$lname = $row['lname'];
			$empID = $row['empID'];
			$email = $row['email'];
			$phoneNum = $row['phoneNum'];
			$password = $row['password'];
			$status = $row['status'];
			$empSince = $row['empSince']; //2012-11-04
		}
		
		$formatted_number = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $phoneNum);
		?>

		<div id="empProfile">
			<div class="container" style="margin-top: 45px; font-family: sans-serif;">
				<h1>Profile</h1>
				<br>
				<p><b>Name: </b><?php echo $fname." ".$lname; ?></p>
				<p><b>Employment Status: </b><?php echo $status;?></p>
				<p><b>Employee Since: </b><?php echo $empSince; ?></p>
				<p><b>Email: </b><?php echo $email; ?></p>
				<p><b>Phone Number: </b><?php echo $formatted_number; ?></p>
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#updatePassModal">Update Password</button>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="updatePassModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle">Update Password</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-container">
							<form action="update_pass_action.php" method="POST">
								
								<div class="form-group">
									<input type="password" class="form-control" name="currentPass" placeholder="Enter Current Password">
								</div>
								<div class="form-group">
									<input type="password" class="form-control" name="newPass" placeholder="New Password">
								</div>
								<div class="form-group">
									<input type="password" class="form-control" name="confirmPass" placeholder="Confirm Password">
								</div>
								<div class="form-group">
									<button type="submit" name="update_pass" class="btn btn-primary">Update Password</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php  

	}else{
		echo '<h1 style="color:blue;text-align:center;margin-top:100px;">You are not signed in</h1>';
	}

	?>

	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<script src="js/script.js"></script>
</body>

<footer>
	<div class = "copyright">
		<div class = "col-md-12 text-center">
			<p>&copy; Copyright 2020. Zero Index Solutions. All Rights Reserved.</p>
		</div>
	</div>
</footer>

</html>