<!DOCTYPE html>
<html>
<head>
	<title>Reviews</title>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-167530546-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-167530546-1');
	</script>

	<!-- Keywords -->
	<meta name="keywords" content="decorators, interior, exterior, builders, construction, va, VA, Virginia, 
		Northern Virginia, NoVA, NOVA, rennovation, addition, bathroom, bedroom, roof, backyard, front yard, 
		sheds, install, open space, space, garden, roofing, floor, flooring, tiles, tiling, paint, painting, 
		spring, special, custom, ideal, modern, traditional, style, stylish, update, decor, colonial, shingles, 
		baseboards, deck, siding, downsprout, storm drain, kbdecorators, knickers, knicker, knickerbocker, 
		knickerbocker decorators, knicker bocker, zero, 1, 2, 3, 4, beds, baths, rooms, ceiling, townhouse, house, 
		condo, apartment, fixture, repair, replace, replacement, fixing, budget friendly, budget, affordable, competitive, 
		customer, satisfaction, customer satisfaction, licensed, bonded, insured, return customers, family owned.">

	<!-- 	viewport -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- 	Font awesome and SuperSlides -->
	<script src="https://kit.fontawesome.com/e294a45d38.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="css/all.min.css">
	<link rel="stylesheet" type="text/css" href="css/superslides.css">
	<!-- Bootstrap CDN -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<!-- Jquery CDN -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
	
</head>
<body>

	<div class="loader">
		<div class="inner">

		</div>
	</div>

	<?php
		require "customer-nav.php";
	?>
	
	<div id="reviews">
		<div class="container">
            <?php
                include("database/config.php");
                $query="select name, header, message from (select * from reviews order by reviewID desc limit 4) x order by reviewID desc;";
                $sql=mysqli_query($conn, $query);
                $reviewInfo = array();
                while ($row_review=mysqli_fetch_assoc($sql))
                    $reviewInfo[]=$row_review;

                echo "<h1>Clients across Northern Virginia trust in our service!</h1>";
                echo "<div class=\"row\">";
                foreach ($reviewInfo as $review) {
                    echo "<div class=\"col-sm\">";
                        echo "<h2>{$review['header']}</h2>";
                        echo "<p>{$review['message']}</p>";
						echo "<img style=\"height:100px;\" src=\"img/icons/user.png\">";
                        echo "<p>{$review['name']}</p>";
                    echo "</div>";
                }
                echo "</div>";
                mysqli_close($conn);
            ?>
		</div>
	</div>

	<script src="js/typed.min.js"></script>
	<script src="js/jquery.superslides.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<script type="text/javascript" src="https://unpkg.com/isotope-layout@3.0.6/dist/isotope.pkgd.min.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
	<script src="js/script.js"></script>

</body>

<footer>
	<div class = "copyright">
		<div class = "col-md-12 text-center">
			<p>&copy; Copyright 2020. Zero Index Solutions. All Rights Reserved.</p>
		</div>
	</div>
</footer>

</html>
