<!DOCTYPE html>
<html>
<head>
	<title>KbDecorators</title>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-167530546-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-167530546-1');
	</script>

	<!-- Keywords -->
	<meta name="keywords" content="decorators, interior, exterior, builders, construction, va, VA, Virginia, 
		Northern Virginia, NoVA, NOVA, rennovation, addition, bathroom, bedroom, roof, backyard, front yard, 
		sheds, install, open space, space, garden, roofing, floor, flooring, tiles, tiling, paint, painting, 
		spring, special, custom, ideal, modern, traditional, style, stylish, update, decor, colonial, shingles, 
		baseboards, deck, siding, downsprout, storm drain, kbdecorators, knickers, knicker, knickerbocker, 
		knickerbocker decorators, knicker bocker, zero, 1, 2, 3, 4, beds, baths, rooms, ceiling, townhouse, house, 
		condo, apartment, fixture, repair, replace, replacement, fixing, budget friendly, budget, affordable, competitive, 
		customer, satisfaction, customer satisfaction, licensed, bonded, insured, return customers, family owned.">

	<!-- 	viewport -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- 	Font awesome and SuperSlides -->
	<script src="https://kit.fontawesome.com/e294a45d38.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="css/all.min.css">
	<link rel="stylesheet" type="text/css" href="css/superslides.css">
	<!-- Bootstrap CDN -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<!-- Jquery CDN -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
	
</head>
<body>

	<div class="loader">
		<div class="inner">

		</div>
	</div>

	<?php
		require "customer-nav.php";
	?>

	<div class="slideContainer">
		<div id="hero">
			<div id="slides">

				<div class = "overlay">

				</div>

				<!-- Image slider  -->
				<div class="slides-container">
					<img src="img/room.jpg" alt="">
					<img src="img/kitchen.jpg" alt="">
					<img src="img/bathroom.jpg" alt="">
				</div>

				<div class="titleMessage">
					<div class="heading">
						<p class="main">Knickerbocker Decorators</p>
					</div>
				</div>

				<nav class="slides-navigation">
					<a href="#" class="next"></a>
					<a href="#" class="prev"></a>
				</nav>

			</div>
		</div>
	</div>

	<div id="about" class="section">
		<div class="container">
			<div class="row" text-align="center">
				<div class="col-md-4 col-sm-6">
					<div class="iconsParagraph">
						<img src="https://img.icons8.com/officel/80/000000/talk-male.png"/>
					</div>
					<h1>Discuss a Project</h1>
					<p>
						At KB Decorators, we take pride in what we do and how we do it. We start the process by getting to know our customers. 
						We don't just ask for your 'wants and needs' and hand you numbers. We hold a discussion with you and understand what 
						you're wanting in detail. Only then do we give you an estimate that would be as close to the real price as possible. 
						By knowing your desired outcome, your taste, and you, we can create the perfect project solution.
					</p>
				</div>
				<div class="col-md-4 hidden-xs hidden-sm">
					<div class="iconsParagraph">
						<img src="https://img.icons8.com/officel/80/000000/us-dollar.png"/>
					</div>
					<h1>Request a Quote</h1>
					<p>
						We put our years of experience to work for you when taking on any project. Being a family owned and operated business, 
						we understand financial obligations and budgeting. That's why we make sure to find the highest quality materials that 
						will endure the test of time at prices that don't break the bank. We take your budge into consideration and always 
						try to find the top tier materials for your property. Knowing our craft lets us deliver unbeatable quality work at 
						unbeatable value.
					</p>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="iconsParagraph">
						<img src="https://img.icons8.com/cute-clipart/80/000000/calendar-10.png"/>
					</div>
					<h1>Schedule a Visit</h1>
					<p>
						You've discussed your dream outcome and gotten a quote and set the budget. Now what? Well, you tell us when you want 
						to see results and we'll make it happen! It's that simple and easy. 😉 We will come to your property on the date and 
						time you tell us and leave you with results that surpass expectations. Complete customer satisfaction is our promise 
						to you and based on our customer feedback, it's a promise we keep.  
					</p>
				</div>
			</div>
		</div>
	</div>

	<script src="js/jquery.superslides.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<script type="text/javascript" src="https://unpkg.com/isotope-layout@3.0.6/dist/isotope.pkgd.min.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
	<script src="js/script.js"></script>

</body>

<footer>
	<div class = "copyright">
		<div class = "col-md-12 text-center">
			<p>&copy; Copyright 2020. Zero Index Solutions. All Rights Reserved.</p>
		</div>
	</div>
</footer>

</html>