<?php 
session_start();
$empID = $_SESSION['empID'];
ini_set('display_errors', 1);
error_reporting(E_ALL);
if (isset($_POST['update_pass'])) {
	require 'database/config.php';

	//Retrieving submitted data
	$submittedPass = $_POST['currentPass'];


	$newPass = $_POST['newPass'];
	$confirmPass = $_POST['confirmPass'];

	if ($newPass != $confirmPass){
		header("Location: home.php?error=passwords_dont_match");
		exit();
	}
	
	//Verify current password
	$sqlTwo = "SELECT email, aes_decrypt(password, 'H+MbQeThWmZq3t6w') from employees where empID = '$empID';";
	$dbPass = mysqli_query($conn, $sqlTwo);
	$currentPass = $dbPass->fetch_array()[1] ?? '';

	$pwdCheck = strcmp($submittedPass, $currentPass); 
	if ($pwdCheck == 0) {
		$sqlUpdatePass = "UPDATE employees SET password = AES_ENCRYPT('$newPass', 'H+MbQeThWmZq3t6w') WHERE empID = '$empID';";

		if (mysqli_query($conn, $sqlUpdatePass)) {
			header("Location: home.php?success=pwUpdated");
			exit();
		} else {
			echo "Error updating password: " . mysqli_error($conn);
		}
	}else{
		header("Location: home.php?error=current_pass_wrong");
		exit();
	}
}
?>