<?php 
if (isset($_POST['logout'])) {
	if (!isset($_SESSION['empID'])){
		session_start();
		session_destroy();
	}
	header("Location: emp.php");
	exit(); 
}
?>