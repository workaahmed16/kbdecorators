<!DOCTYPE html>
<html>
<head>
	<title>Review Form</title>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-167530546-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-167530546-1');
	</script>

	<!-- 	viewport -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- 	Font awesome and SuperSlides -->
	<script src="https://kit.fontawesome.com/e294a45d38.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="css/all.min.css">
	<link rel="stylesheet" type="text/css" href="css/superslides.css">
	<!-- Bootstrap CDN -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<!-- Jquery CDN -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
	
</head>
<body>

	<div class="loader">
		<div class="inner">

		</div>
	</div>

	<?php
		require "customer-nav.php";
	?>

	
	<div id ="contact">
		<div class="container">
			<h1 style="text-align: center;">Leave a Review</h1>
			<p style="text-align: center;">Thank you for using KbDecorators! It was a pleasure to work with you.<br>Please fill out this from to leave
            a review of our service.
			</p>
			<br>
			<form action = "reviewForm.php" method = "post">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="Name">Name</label>
						<input type="text" class="form-control" name="name" id="reviewName" placeholder="Name" required>
					</div>
					<div class="form-group col-md-6">
						<label for="Email">Email</label>
						<input type="email" class="form-control" name="email" id="reviewEmail" placeholder="Email" required
							pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-12">
                        <label for="Header">Header</label>
						<input type="text" class="form-control" name="header" id="reviewHeader" placeholder="Header" required>
                    </div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-12">
						<label for="Message">Message</label>
						<textarea class="form-control" name="message" id="reviewMessage" rows="5" cols="50" required></textarea>
					</div>
				</div>
				<br>
				<button type="submit" name="submit" class="btn btn-primary">Submit</button>

				<?php
					include("database/config.php");
					// If the form has been submitted, stay on same page but process the data
					if(isset($_POST["submit"])) {
							
						$name = $_POST["name"];
                        $email = $_POST["email"];
                        $header = $_POST["header"];
                        $message = $_POST["message"];
                        
                        //Send info to the database
                        $query = "INSERT INTO reviews (email, name, header, message) VALUES ('$email', '$name', '$header', '$message')";
                        $sql=mysqli_query($conn, $query);

                        //Snackbar for confirmation
                        if($sql) {
                            echo "<div id='snackbar'>Review sent!</div>";
                        } else {
                            echo "<div id='snackbar'>Error! Review not sent sent!</div>";
                        }
					}
				?>
			</form>
		</div>
	</div>

	<script src="js/typed.min.js"></script>
	<script src="js/jquery.superslides.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<script type="text/javascript" src="https://unpkg.com/isotope-layout@3.0.6/dist/isotope.pkgd.min.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
	<script src="js/script.js"></script>

</body>

<footer>
	<div class = "copyright">
		<div class = "col-md-12 text-center">
			<p>&copy; Copyright 2020. Zero Index Solutions. All Rights Reserved.</p>
		</div>
	</div>
</footer>

</html>